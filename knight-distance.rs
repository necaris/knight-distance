use std::cmp;
use std::io::{self, Write};
use std::collections::HashMap;
use std::vec::Vec;

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
struct Coordinate {
    x: i32,
    y: i32
}

fn possible_moves(c: &Coordinate) -> [Coordinate; 8] {
    [Coordinate{x: c.x + 2, y: c.y + 1},
     Coordinate{x: c.x - 2, y: c.y + 1},
     Coordinate{x: c.x + 2, y: c.y - 1},
     Coordinate{x: c.x - 2, y: c.y - 1},
     Coordinate{x: c.x + 1, y: c.y + 2},
     Coordinate{x: c.x - 1, y: c.y + 2},
     Coordinate{x: c.x + 1, y: c.y - 2},
     Coordinate{x: c.x - 1, y: c.y - 2}]
}

fn get_next_positions(starting: Vec<Coordinate>, target: &Coordinate, visited: &mut HashMap<Coordinate, i32>,
                      iteration: i32, num_tries: i32) -> i32 {

    let mut next_positions = Vec::with_capacity(32768);
    for pos in starting {
        let possibilities = possible_moves(&pos);
        for possibility in possibilities.iter() {
            if !visited.contains_key(possibility) {
                next_positions.push(possibility.clone())
            }
            let distance = match visited.get(possibility) {
                Some(x) => *x,
                None => iteration + 1
            };
            visited.insert(possibility.clone(), cmp::min(distance, iteration + 1));
        }
    }

//    if iteration % 10 == 0 {
        print!("\ri: {} next: {} visited: {}",
               iteration,
               // start_positions.len(),
               next_positions.len(),
               visited.len());
        io::stdout().flush().unwrap();
//    }

    if let Some(steps) = visited.get(target) {
        return *steps;
    }
    if iteration >= num_tries {
        return -2;
    }

    return get_next_positions(next_positions, target, visited, iteration + 1, num_tries);
}

fn formulaic_solution(c: Coordinate) -> i32 {
    let delta = c.x - c.y;
    // NOTE rust does truncating integer division so we don't need
    // a `floor()` function
    if c.y > delta {
        (2 * (c.x - delta) / 3) + delta
    } else {
        delta - (2 * ((delta - c.y) / 4))
    }
}

fn solution(start: &Coordinate, target: &Coordinate, num_tries: i32) -> i32 {
    let mut visited: HashMap<Coordinate, i32> = HashMap::with_capacity(4000000);
    let start_positions = vec![start.clone()];

    return get_next_positions(start_positions, target, &mut visited, 0, num_tries);
}

fn main() {
    let start = Coordinate{x: 0, y: 0};
    let target = Coordinate{x: 1773, y: -457};
    println!("Coordinate is: {:?}", start);
    println!(" - aiming for {:?}", target);
    println!(" - solution is {}", solution(&start, &target, 100000000));
}
