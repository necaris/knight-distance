/* node:true */
"use strict";
let util = require("util");
let LIMIT = 100000000;


function possibleMoves(x, y) {
    let possibilities = [
        [x + 2, y + 1],
        [x - 2, y + 1],
        [x + 2, y - 1],
        [x - 2, y - 1],
        [x + 1, y + 2],
        [x - 1, y + 2],
        [x + 1, y - 2],
        [x - 1, y - 2]
    ];
    return possibilities;
}

function solution(A, B) {
    let visited = {};
    let targetX = A, targetY = B;
    let targetSquare = [A, B];
    let startPositions = [[0, 0]];

    for (var i = 0; i < LIMIT; i++) {
        let nextPositions = [];
        for (var j = 0; j < startPositions.length; j++) {
            let coords = startPositions[j];
            let x = coords[0], y = coords[1];
            let possibilities = possibleMoves(x, y);
            for (var k = 0; k < possibilities.length; k++) {
                let move = possibilities[k];
                let moveX = move[0], moveY = move[1];
                if (!visited[move]) {
                    nextPositions.push(move);
                }
                let distance = visited[move] || i + 1;
                visited[move] = Math.min(distance, i + 1);
            }
        }
        process.stdout.write(util.format(
            "\ri: %d start: %d next: %d",
            i, startPositions.length, nextPositions.length));
        if (visited[targetSquare]) {
            return visited[targetSquare];
        }

        startPositions = nextPositions;
    }

    return -2;
}

console.log(solution(4476, -639));
