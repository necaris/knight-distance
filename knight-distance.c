#include <stdio.h>
#include <stdlib.h>

#define MIN(a, b) (a > b) ? a : b
#define INITIAL_DIMENSION 131072

int* possible_moves(int x, int y) {
  static int possibilities_array[8][2];
  possibilities_array[0][0] = x + 2;
  possibilities_array[0][1] = y + 1;
  possibilities_array[1][0] = x - 2;
  possibilities_array[1][1] = y + 1;
  possibilities_array[2][0] = x + 2;
  possibilities_array[2][1] = y - 1;
  possibilities_array[3][0] = x - 2;
  possibilities_array[3][1] = y - 1;

  possibilities_array[4][0] = x + 1;
  possibilities_array[4][1] = y + 2;
  possibilities_array[5][0] = x - 1;
  possibilities_array[5][1] = y + 2;
  possibilities_array[6][0] = x + 1;
  possibilities_array[6][1] = y - 2;
  possibilities_array[7][0] = x - 1;
  possibilities_array[7][1] = y - 2;
  return possibilities_array[0];
}

int get_value_at(int** visited, int x, int y) {
  if ((x > (INITIAL_DIMENSION / 2)) || (y > (INITIAL_DIMENSION / 2))) {
    printf("Oh my! So big so soon? x: %d, y: %d, dimension: %d", x, y, INITIAL_DIMENSION);
    return -1;
  }
  // Adjust x, y to account for the negative spaces
  int lookup_x = x + (INITIAL_DIMENSION / 2);
  int lookup_y = y + (INITIAL_DIMENSION / 2);
  return visited[lookup_x][lookup_y];
}

void set_value_at(int** visited, int x, int y, int value) {
  if ((x > (INITIAL_DIMENSION / 2)) || (y > (INITIAL_DIMENSION / 2))) {
    printf("Oh my! So big so soon? x: %d, y: %d, dimension: %d", x, y, INITIAL_DIMENSION);
    return;
  }
  // Adjust x, y to account for the negative spaces
  int lookup_x = x + (INITIAL_DIMENSION / 2);
  int lookup_y = y + (INITIAL_DIMENSION / 2);
  visited[lookup_x][lookup_y] = value;
}

int solution(int x, int y) {
  int i, j, k, q;
  int num_tries = 100000;
  // int dimension = INITIAL_DIMENSION;
  int** visited;
  visited = (int**)calloc(INITIAL_DIMENSION, sizeof(int*));

  for(i = 0; i < INITIAL_DIMENSION; i++) {
    visited[i] = (int*)calloc(INITIAL_DIMENSION, sizeof(int));
  }

  int n_start_positions = 0;
  int start_positions[INITIAL_DIMENSION][2];
  int n_next_positions = 0;
  int next_positions[INITIAL_DIMENSION][2];


  // initialize
  n_start_positions = 1;
  start_positions[0][0] = 0;
  start_positions[0][1] = 0;
  // printf("initialized!\n");
  for (i = 0; i < num_tries; i++) {
    // n_next_positions = 0;
    // printf("beginning loop %d\n", i);
    for (j = 0; j < n_start_positions; j++) {
      // printf(" - inner loop %d\n", j);
      int start_x = start_positions[j][0];
      int start_y = start_positions[j][1];
      // printf("- read starts %d, %d\n", start_x, start_y);
      int* possibilities = possible_moves(start_x, start_y);
      // printf("- got possibilities\n");
      for (k = 0; k < 8; k++) {
        int move_x = *(possibilities + (2 * k) + 0);
        int move_y = *(possibilities + (2 * k) + 1);
        // printf(" -- possible move %d: %d, %d\n", k, move_x, move_y);
        if (get_value_at(visited, move_x, move_y) == 0) {
          // printf("   (not visited) (n_next_positions: %d)\n", n_next_positions);
          next_positions[n_next_positions][0] = move_x;
          next_positions[n_next_positions][1] = move_y;
          n_next_positions++;
        }
        int current = get_value_at(visited, move_x, move_y);
        int distance = (current == 0) ? i + 1 : current;
        set_value_at(visited, move_x, move_y, MIN(distance, i + 1));
      }
    }
    if (get_value_at(visited, x, y) > 0) {
      return get_value_at(visited, x, y);
    }
    // status report
    fprintf(stderr, "\ri: %d start: %d next: %d", i, n_start_positions, n_next_positions);
    // copy next_positions into start_positions and zero it out
    for (q = 0; q < n_next_positions; q++) {
      start_positions[q][0] = next_positions[q][0];
      start_positions[q][1] = next_positions[q][1];
      next_positions[q][0] = 0; next_positions[q][1] = 0;
    }
    n_start_positions = n_next_positions;
    n_next_positions = 0;
  }

  return -2;
}

int main(int argc, char** argv) {
  int x = 4476;
  int y = -639;
  printf("\nyou can get to %d, %d in %d moves!\n", x, y, solution(x, y));
}
