from __future__ import print_function
import sys
import math
try:
    range = xrange
except NameError:
    pass

def possible_moves(x, y):
    possibilities = [
        (x + 2, y + 1),
        (x - 2, y + 1),
        (x + 2, y - 1),
        (x - 2, y - 1),
        (x + 1, y + 2),
        (x - 1, y + 2),
        (x + 1, y - 2),
        (x - 1, y - 2),
    ]
    return possibilities

def solution(A, B):
    visited = {}
    target_square = (A, B)
    start_positions = [(0, 0)]
    for i in range(100000000):
        next_positions = []
        for x, y in start_positions:
            for move in possible_moves(x, y):
                if move not in visited:
                    next_positions.append(move)
                distance = visited.get(move, i + 1)
                visited[move] = min(distance, i + 1)

        # print len(start_positions), len(next_positions), len(visited)
        print("\ri:{0:>5d} start: {1:>6d} next: {2:>6d} visited: {3:>6d}".format(i, len(start_positions), len(next_positions), len(visited)), end="", file=sys.stderr)
        if target_square in visited:
            return visited[target_square]

        start_positions = next_positions

    else:
        return -2

def solution_formula(A, B):
    delta = A - B
    if B > delta:
        return (2 * math.floor((B - delta) / 3)) + delta
    else:
        return delta - (2 * math.floor((delta - B) / 4))


if len(sys.argv) < 3:
    x, y = 4476, -639
else:
    x, y = int(sys.argv[1]), int(sys.argv[2])
print("\n{}\n".format(solution(x, y)))
